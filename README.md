# DISCLAIMER

The tool currently receives some overhaul and the documentation here is outdated

# Absolem

This tools aims to provide you with a set of features to backup and restore or transfer data between Elasticsearch instances.

Elasticsearch < 6.0 has never been tested (and probably never will be).

Absolem is based on .net core 2.1 => https://dotnet.microsoft.com/download

# Usage

| Argument | Info |  |
|---|---|---|
| Position 0 | Source to retrieve the data from. e.g. http://localhost:9200 or mybackup.zip | Required 
| Position 1 | Destination to copy the data to. This can be another Elasticsearch instance or a zipfile. If no destination is provided ABsolem will create a folder "archives" and place a new zip file there. | Optional
| -f | List of indices to export/import. This argument supports regex, so you could provide a name like -f "([0-9]{6})_MonthlyTransactions" to match every index like 201812_MonthlyTransactions | Optional
| --Force | Force overwrite of existing data. For Elasticsearch this means an existing index will be deleted before import. | Optional
| --LogLevel | Set log level to one of the specified: Verbose, Debug, Information, Warning, Error, Fatal | Optional
| --Save | With -Save Absolem will create a file containing the arguments for reuse | Optional
| --No-Aliases | Don't import aliases to the destination | Optional
| @\<filename\> | Load arguments from the provided file. This is very helpful to reuse arguments or keep your application call short. | Optional

# Examples

## Dump an Elasticsearch instance to a zip file
 
``` dotnet Absolem.dll http://localhost:9200 archives/mydump.zip ```

## Dump specific indices

``` dotnet Absolem.dll http://localhost:9200 archives/mydump.zip -f "importantIndex" -f "the_other_one_I_need" ```

## Restore data from a zip file

``` dotnet Absolem.dll archives/mydump.zip http://localhost:9200 ```

## Transfer data between two instances

``` dotnet Absolem.dll http://localhost:9200 http://localhost:9201 ```

# LICENSE

MIT License

Copyright (c) 2017 Creative Tim

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
