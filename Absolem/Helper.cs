using System;
using System.Text.RegularExpressions;

namespace Absolem
{
    public static class Helper
    {
        public static string SecondsToHumanReadable(long secs)
        {
            TimeSpan seconds = TimeSpan.FromSeconds(secs);

            return seconds.ToString("c");
        } 
        
        public static long ConvertToBytes(string size)
        {
            string[] sizes = { "b", "kb", "mb", "gb", "tb" };

            double bytes = double.Parse(Regex.Match(size, @"\d+(\.\d{1,2})?").Value);
            string ext = Regex.Match(size, @"\D+$").Value;
            for (int i = 0; i < sizes.Length && sizes[i] != ext; i++)
            {
                bytes *= 1024;
            }            

            return (long) bytes;
        }
    }
}