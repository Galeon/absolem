using System.Collections.Generic;

namespace Absolem
{
    public class RunArguments
    {        
        public string Source { get; set; }
           
        public string Destination { get; set; }
                 
        public bool Force { get; set; }

        public List<string> Filter { get; set; } = new List<string>();

        public bool NoAliases { get; set; }

        public int CpuCnt { get; set; }

        public bool Gui { get; set; }
        
        public int ChunkSize { get; set; }
    }
}