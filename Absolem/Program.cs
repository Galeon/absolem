﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using McMaster.Extensions.CommandLineUtils;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace Absolem
{
    /**
     * Arguments
     * src         Source to retrieve the data from
     * dest        Destination to copy the data to
     *
     * indices     List of indices to export/import
     * force       Force overwrite of existing data
     * 
     * verbose     Log level debug    
     * quiet       log level warning
     */
    [Command(Name = "Absolem", Description = "Backup and restore Elasticsearch instances", ResponseFileHandling = ResponseFileHandling.ParseArgsAsSpaceSeparated)]
    class Program
    {
        /***************************************************************************************************************
         * ARGUMENTS
         **************************************************************************************************************/
        
        [Argument(0, Description = "Source of the data, e.g. http://mycluster:9200 or mybackup.zip")]
        [Required]
        private string Source { get; }
        
        [Argument(1, Description = "Target for the data, e.g. http://localhost:9200 or backup.zip. If this argument is not given a ZIP archive will be created.")]
        private string Destination { get; }

        [Option(Description = "Log level, valid values: Verbose, Debug, Information, Warning, Error, Fatal")]
        private LogEventLevel LogLevel { get; } = LogEventLevel.Information;

        [Option("--force", Description = "Force overwrite an existing destination")]
        private bool Force { get; }

        [Option("-f|--filter", Description = "A list of Indices to backup/restore. This may contain multiple values and is capable of regex. e.g. -f Customers -f \"([0-9]{6})_logs\"")]
        private List<string> Filter { get; }

        [Option("--no-aliases|-na", Description = "Don't import aliases to the destination")]
        private bool NoAliases { get; }

        [Option("--save|-s", Description = "Save the passed arguments as a reusable config file")]
        private bool Save { get; }

        [Option("--parallel|-p", Description = "Maximum amount of threads to use for parallel work")]
        private int CpuCnt { get; } = 4;

        [Option("--gui", Description = "Enables a GUI-style mode instead of just streaming the log infos")]
        private bool Gui { get; } = false;
        
        [Option("--chunk-size", Description = "Number of documents to fetch per chunk from an ES")]
        private int ChunkSize { get; } = 500;

        /***************************************************************************************************************
         * PROPERTIES
         **************************************************************************************************************/

        private static readonly LoggingLevelSwitch LevelSwitch = new LoggingLevelSwitch();
        
        private static readonly Version Version = new Version(2, 0, 1); 
        
        /// <summary>
        /// The folder for temporary data
        /// </summary>
        public const string TmpFolder = "tmp";

        /// <summary>
        /// The default folder for zip exports
        /// </summary>
        public const string ArchiveFolder = "archives";

        /***************************************************************************************************************
         * METHODS
         **************************************************************************************************************/
        
        public static void Main(string[] args)  => CommandLineApplication.Execute<Program>(args);
        
        public static Logger GetLogger()
        {
            return new LoggerConfiguration()
                .WriteTo.Console()
                .MinimumLevel.ControlledBy(LevelSwitch)
                .CreateLogger();
        }
        
        /// <summary>
        /// Gets called when when the application launches
        /// </summary>
        public void OnExecute()
        {
            GetLogger().Information("Absolem v{version}", Version.ToString());
            
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            
            LevelSwitch.MinimumLevel = LogLevel;
            
            ValidateArguments();
              
            if (Save)
            {                
                SaveArguments();
            }
            
            new Run(new RunArguments() {
                Source      = Source,
                Destination = Destination,
                Force       = Force,
                Filter      = Filter,
                NoAliases   = NoAliases,
                CpuCnt      = CpuCnt,
                Gui         = Gui,
                ChunkSize   = ChunkSize
            }).Start();
        }

        /// <summary>
        /// Does more detailed or complex tests on input arguments/options 
        /// </summary>
        private void ValidateArguments()
        {
            // Validate "Source" it should somehow match an http URI or a file ending with .zip
            if (!Regex.IsMatch(Source, "^http|\\.zip$"))
            {
                Console.WriteLine("Source must be a valid http URI starting with 'http' or a zip file ending with '.zip'");
                Environment.Exit(-1);
            }
            
            // Validate "Destination" it should somehow match an http URI or a file ending with .zip
            if (Destination != null && !Regex.IsMatch(Destination, "^http|\\.zip$"))
            {
                Console.WriteLine("Destination must be a valid http URI starting with 'http' or a zip file ending with '.zip'");
                Environment.Exit(-1);
            }

            if (Source.EndsWith(".zip") && Destination.EndsWith(".zip"))
            {
                Console.WriteLine("Source and destination can't both be a zip file");
                Environment.Exit(-1);
            }

            if (Source.Equals(Destination))
            {
                Console.WriteLine("Source and destination must have different values'");
                Environment.Exit(-1);
            }
        }

        /// <summary>
        /// Save the provided argument to a file which can be reused for further calls
        /// </summary>
        private void SaveArguments()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0} ", Source);
            sb.AppendFormat("{0} ", Destination);

            if (LogLevel != LogEventLevel.Information)
            {
                sb.AppendFormat("-l {0} ", LogLevel);
            }

            if (Force)
            {
                sb.Append("--Force ");
            }

            if (NoAliases)
            {
                sb.Append("--No-Aliases ");
            }

            if (CpuCnt != 4)
            {
                sb.AppendFormat("--parallel {0}", CpuCnt);
            }

            if (ChunkSize != 500)
            {
                sb.AppendFormat("--chunk-size {0}", ChunkSize);
            }
                
            Filter?.ForEach(f => sb.AppendFormat(" -f \"{0}\" ", f));

            string argsFile = $"{DateTime.Now:s}.args";
            File.WriteAllText(argsFile, sb.ToString());
                
            GetLogger().Information("Wrote argument file to {argsFile}. To use call with absolem @{argsFile}", argsFile);            
        }
    }
}