using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Absolem.handler;
using Serilog.Core;

namespace Absolem
{
    public class Run
    {
        /***************************************************************************************************************
         * PROPERTIES
         **************************************************************************************************************/
        
        private readonly Logger _log = Program.GetLogger();

        private readonly RunArguments _arguments;

        private readonly IInputHandler _src;

        private readonly IOutputHandler _dest;

        private readonly CliInterface _cli;

        /***************************************************************************************************************
         * METHODS
         **************************************************************************************************************/
        
        public Run(RunArguments args)
        {
            _cli = new CliInterface(args.Gui, args.CpuCnt);
            
            _arguments = args;
            
            // Create source
            _log.Verbose("Connecting to source");
            _src = ConstructInputFromUri(args);
            _src.SetCli(_cli);
            
            // Create destination
            _log.Verbose("Connecting to target");
            _dest = args.Destination != null ? 
                ConstructOutputFromUri(args.Destination) : 
                ConstructOutputFromUri($"archives/{_src.GetClusterName()}-{DateTime.Now:s}.zip"); // Default destination
            
            _dest.SetCli(_cli);
            
            // Create application directories
            Directory.CreateDirectory(Program.ArchiveFolder);
            Directory.CreateDirectory(Program.TmpFolder);
        }
        
        /// <summary>
        /// Start the processing of the data and pass them from the source to the destination 
        /// </summary>
        public void Start()
        {
            // Gather all indices
            _src.StartExport();
            List<Index> indices = _src.GetIndices(_arguments.Filter);
            
            _dest.StartImport();
            
            Parallel.For (0, indices.Count,
                new ParallelOptions { MaxDegreeOfParallelism = _arguments.CpuCnt },
                i =>
            {
                Index idx = indices[i];

                // Start index work
                _cli.StartWorkIdx();
                _src.StartIndexExport(idx);
                _dest.StartIndexImport(idx);
                
                // Settings
                string settingsJson = _src.GetSettings(idx);
                if (!_dest.AddIndex(idx, settingsJson, _arguments.Force))
                {
                    _cli.AddErr(idx.Name, "Could not create index");
                    return;
                }

                // Mappings
                string mappingsJson = _src.GetMappings(idx);
                _dest.AddMappings(idx, mappingsJson);

                // Aliases
                if (!_arguments.NoAliases)
                {
                    string aliasJson = _src.GetAliases(idx);
                    if (!string.IsNullOrWhiteSpace(aliasJson))
                    {
                        _dest.AddAliases(idx, aliasJson);
                    }
                }

                // Sync data from source to destination
                _src.GetData(idx, _dest);

                // Finish index
                _src.FinishIndexExport(idx);
                _dest.FinishIndexImport(idx);
                _cli.FinishedIdx();
            });
            
            _dest.FinishImport();
            _src.FinishExport();
            
            _cli.Print();
        }

        /// <summary>
        /// Instantiate a handler based on the provided uri
        /// </summary>
        /// <param name="uri">The URI to create a handler for</param>
        /// <returns>An instantiated handler</returns>
        private IOutputHandler ConstructOutputFromUri(string uri)
        {
            // Instantiate Zip file handler
            if (uri.EndsWith(".zip"))
            {
                return new FileHandler(uri);
            }
            
            // Instantiate Elasticsearch handler
            if (uri.StartsWith("http"))
            {
                return new EsOutputHandler(uri);
            }
            
            _log.Fatal("Could not find a matching output handler for {uri}", uri);
            
            Environment.Exit(-1);

            return null;
        }
        
        /// <summary>
        /// Instantiate a handler based on the provided uri
        /// </summary>
        /// <param name="uri">The URI to create a handler for</param>
        /// <returns>An instantiated handler</returns>
        private IInputHandler ConstructInputFromUri(RunArguments args)
        {
            string uri = args.Source;
            
            // Instantiate Zip file handler
            if (uri.EndsWith(".zip"))
            {
                if (File.Exists(uri)) return new FileHandler(uri);
                
                _log.Fatal($"Could not find file {uri}");
                Environment.Exit(-1);

                return null;
            }
            
            // Instantiate Elasticsearch handler
            if (uri.StartsWith("http"))
            {
                return new EsInputHandler(uri, args.ChunkSize);
            }
            
            _log.Fatal("Could not find a matching input handler for {uri}", uri);
            
            Environment.Exit(-1);

            return null;
        }
    }
}