using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Timers;
using Serilog.Core;

namespace Absolem
{
    public class CliInterface
    {
        private readonly Logger _log = Program.GetLogger();
        
        public string Source { get; set; } = "";

        public string Destination { get; set; } = "";

        private long _idxTodo = 0;
        private long _idxActive = 0;
        private long _idxDone = 0;

        private long _docsTodo = 0;
        private long _docsDone = 0;

        private readonly int _threads;

        private readonly Stopwatch _sw = Stopwatch.StartNew();
        
        private readonly List<string> _warn = new List<string>();
        private readonly List<string> _err = new List<string>();
        private readonly List<string> _info = new List<string>();

        private readonly bool _gui;

        private const double TimerInterval = 1000;

        public CliInterface(bool gui, int threads)
        {
            _threads = threads;
            _gui = gui;

            if (_gui)
            {
                Timer timer = new Timer(TimerInterval);
                timer.Elapsed += OnTimedEvent;
                timer.Start();
            }
        }
        
        public void AddWarn(string idx, string warn)
        {
            string msg = $"[{DateTime.Now.ToShortTimeString()}][{idx}] {warn}";
            
            if (_gui) {
                _warn.Add(msg);
            }
            else
            {
                _log.Warning(msg);
            }
        }

        public void AddErr(string idx, string err)
        {
            string msg = $"[{DateTime.Now.ToShortTimeString()}][{idx}] {err}";

            if (_gui)
            {
                _err.Add(msg);
            }
            else
            {
                _log.Error(msg);
            }
        }

        public void AddInfo(string idx, string info)
        {
            string msg = $"[{DateTime.Now.ToShortTimeString()}][{idx}] {info}";

            if (_gui)
            {
                _info.Add(msg);

                if (_info.Count > 10)
                {
                    _info.RemoveAt(0);
                }
            }
            else
            {
                _log.Information(msg);
            }
        }


        public void AddIdx()
        {
            AddIdx(1);
        }
        
        public void AddIdx(int amount)
        {
            _idxTodo += amount;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void StartWorkIdx()
        {
            _idxTodo--;
            _idxActive++;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void FinishedIdx()
        {
            _idxActive--;
            _idxDone++;
        }

        public void AddDocs(long docs)
        {
            _docsTodo += docs;
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void FinishedWorkDoc(long docs)
        {
            _docsTodo -= docs;
            _docsDone += docs;
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            Print();
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Print()
        {
            if (!_gui)
            {
                return;
            }

            try
            {
                Console.Clear();
                Console.WriteLine($"Source: {Source} Dest: {Destination}");
                Console.WriteLine(
                    $"Max Threads: {_threads} Idx: {_idxTodo,4}/{_idxActive,4}/{_idxDone,4} Docs: {_docsTodo,8}/{_docsDone,8}");

                double dps = 0;
                if (_docsDone > 0)
                {
                    dps = _docsDone / (_sw.ElapsedMilliseconds / 1000.0);
                }

                Console.WriteLine($"Docs per second: {dps:0.00} Time elapsed: {_sw.Elapsed:c}");
                Console.WriteLine("INFO:");
                lock (_info)
                {
                    foreach (string s in _info) Console.WriteLine(s);
                }

                Console.WriteLine("WARN:");
                lock (_warn)
                {
                    foreach (string s in _warn) Console.WriteLine(s);
                }

                Console.WriteLine("ERR:");
                foreach (string s in _err.GetRange(0, Math.Min(_err.Count, 10))) Console.WriteLine(s);
            }
            catch (Exception e)
            {
                this.AddErr("", e.Message);
            }
        }
    }
}