using System.IO;
using System.Text;
using Ionic.Zlib;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Serilog.Core;

namespace Absolem
{
    public class Index
    {
        private readonly Logger _log = Program.GetLogger();
        
        public string Name { get; set; }
        
        public long Documents { get; set; }
        
        public long Size { get; set; }

        public Index(string name, string documents, string size)
        {
            Name      = name;
            Documents = long.Parse(documents);
            Size      = Helper.ConvertToBytes(size);
        }

        public Index(StreamReader gzipJson)
        {
            using MemoryStream mem = new MemoryStream();
            new GZipStream(gzipJson.BaseStream, CompressionMode.Decompress).CopyTo(mem);
            dynamic obj = JsonConvert.DeserializeObject(Encoding.UTF8.GetString(mem.ToArray()));
            
            Name      = obj.Name;

            _log.Information($"Loading index {Name}");
            
            try
            {
                Documents = obj.Documents;
            }
            catch (RuntimeBinderException)
            {
                Documents = 0;
            }

            try
            {
                Size = obj.Size;
            }
            catch (RuntimeBinderException)
            {
                Size = 0;
            }
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}