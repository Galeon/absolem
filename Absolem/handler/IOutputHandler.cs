namespace Absolem.handler
{
    /// <summary>
    /// Contains all the methods required for handling the output of ES data
    /// </summary>
    public interface IOutputHandler : IHandler
    {
        /// <summary>
        /// Add the basic index structure to the output format 
        /// </summary>
        /// <param name="idx">The name of the index</param>
        /// <param name="settingsJson">The settings for the index</param>
        /// <param name="force">If true, an preexisting index should be overwritten</param>
        /// <returns>true on success</returns>
        bool AddIndex(Index idx, string settingsJson, bool force);

        /// <summary>
        /// Add the mappings for an index
        /// </summary>
        /// <param name="idx">The name of the index</param>
        /// <param name="mappingsJson">The JSON for the mappings</param>
        void AddMappings(Index idx, string mappingsJson);

        /// <summary>
        /// Add aliases for the index 
        /// </summary>
        /// <param name="idx">The name of the index</param>
        /// <param name="aliasJson">The JSON for the alises</param>
        void AddAliases(Index idx, string aliasJson);

        /// <summary>
        /// Add and actual data entity to the index
        /// </summary>
        /// <param name="idx">The index to add the data to</param>
        /// <param name="id">The ID of the entity</param>
        /// <param name="source">The JSON source</param>
        void AddData(string idx, string id, string source);

        /// <summary>
        /// Gets fired before the import of a specific index happens. This give the class time to make preperations
        /// </summary>
        /// <param name="idx">The name of the index which will be imported next</param>
        void StartIndexImport(Index idx);

        /// <summary>
        /// Gets fired when the import of an index is done
        /// </summary>
        /// <param name="idx">The name of the index</param>
        void FinishIndexImport(Index idx);
        
        /// <summary>
        /// Gets fired when the whole import process starts
        /// </summary>
        void StartImport();

        /// <summary>
        /// Gets fire when the whole import process is done
        /// </summary>
        void FinishImport();
    }
}