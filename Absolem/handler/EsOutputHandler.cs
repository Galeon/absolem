using System;
using System.Collections.Generic;
using System.Linq;
using Elasticsearch.Net;
using Nest;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog.Core;

namespace Absolem.handler
{
    public class EsOutputHandler : IOutputHandler
    {
        /***************************************************************************************************************
         *  Properties
         **************************************************************************************************************/
        
        private readonly Logger _log = Program.GetLogger();
        
        private readonly ElasticClient _hlc;

        private CliInterface _cli;

        private readonly Dictionary<string, string> _types = new Dictionary<string, string>();
        
        private readonly string _uri;
        
        /***************************************************************************************************************
         *  Methods
         **************************************************************************************************************/

        public EsOutputHandler(string uri)
        {
            ConnectionSettings settings = new ConnectionSettings(new Uri(uri));

            _hlc = new ElasticClient(settings);
            IPingResponse pingResponse = _hlc.Ping();

            if (!pingResponse.ApiCall.Success)
            {
                _log.Fatal("Could not connect to {0}", uri);
                Environment.Exit(1);
            }

            _uri = uri;
        }

        public void SetCli(CliInterface cli)
        {
            _cli = cli;

            IClusterStatsResponse stats = _hlc.ClusterStats(new ClusterStatsRequest());
            string clusterName    = stats.ClusterName;
            string clusterVersion = stats.Nodes.Versions.Min();

            _cli.Source = $"{_uri} {clusterVersion}";
            _cli.AddInfo("", $"Cluster \"{clusterName}\" v {clusterVersion}");
        }
        
        public string GetClusterName()
        {
            return _hlc.ClusterStats(new ClusterStatsRequest()).ClusterName;
        }

        public bool AddIndex(Index idx, string settingsJson, bool force)
        {
            if (force && _hlc.IndexExists(idx.Name).Exists)
            {
                _cli.AddInfo(idx.Name, "Force set to true, removing existing index first");
                _hlc.DeleteIndex(new DeleteIndexRequest(idx.Name));
            } 
            
            JObject settingsObj = JObject.Parse(settingsJson);

            int shards = settingsObj[idx.Name]["settings"]["index"]["number_of_shards"].Value<int>();
            int replicas = settingsObj[idx.Name]["settings"]["index"]["number_of_replicas"].Value<int>();

            ICreateIndexResponse response = _hlc.CreateIndex(idx.Name, 
                r => r.Settings(settings => settings.NumberOfShards(shards).NumberOfReplicas(replicas))
            );

            if (response.ApiCall.HttpStatusCode != 200)
            {
                _cli.AddErr(idx.Name, $"AddIndex: {response.ApiCall.HttpStatusCode}: {response.ServerError?.Error}");
                return false;
            }

            return true;
        }

        public void AddMappings(Index idx, string mappingsJson)
        {
            JObject mappingObj = JObject.Parse(mappingsJson);

            List<JToken> children = mappingObj[idx.Name]["mappings"].Children().ToList();

            if (children.Count > 1)
            {
                _cli.AddErr(idx.Name, "Multiple mappings defined, this is not supported anymore. " +
                                      "Please remove the unused mappings before hand.");
                return;
            }
            
            if (children.Count == 0)
            {
                _cli.AddWarn(idx.Name,"No mapping data");
                
                return;
            }

            JProperty child = (JProperty) children[0];
            _cli.AddInfo(idx.Name, "Create mapping");

            _types.TryAdd(idx.Name, child.Name);
            
            StringResponse response = _hlc.LowLevel.IndicesPutMapping<StringResponse>(idx.Name, child.Name,
                JsonConvert.SerializeObject(mappingObj[idx.Name]["mappings"]));

            if (response.ApiCall.HttpStatusCode != 200)
            {
                _cli.AddErr(idx.Name, $"AddMapping: {response.ApiCall.HttpStatusCode}: {response.Body}");
            }
        }

        public void AddAliases(Index idx, string aliasJson)
        {
            dynamic aliases = JsonConvert.DeserializeObject(aliasJson);

            Dictionary<string,dynamic> aliasList = aliases[idx.Name]["aliases"].ToObject<Dictionary<string, dynamic>>();
            foreach (KeyValuePair<string, dynamic> alias in aliasList)
            {
                _cli.AddInfo(idx.Name,$"Adding alias {alias.Key}");
                StringResponse response = _hlc.LowLevel.IndicesPutAlias<StringResponse>(idx.Name, alias.Key, JsonConvert.SerializeObject(alias.Value));
            
                if (response.ApiCall.HttpStatusCode != 200)
                {
                    _cli.AddErr(idx.Name, $"AddAlias: {response.ApiCall.HttpStatusCode}: {response.Body}");
                }                
            }
        }

        public async void AddData(string idx, string id, string source)
        {
            StringResponse response = await _hlc.LowLevel.IndexAsync<StringResponse>(idx, _types[idx], id, source);

            _cli.FinishedWorkDoc(1);
            
            if (response.ApiCall.HttpStatusCode != 201)
            {
                _cli.AddErr(idx, $"AddData: {response.ApiCall.HttpStatusCode}: {response.Body}");
            }            
        }

        public void StartIndexImport(Index idx) => _cli.AddInfo(idx.Name, "Start work");

        public void FinishIndexImport(Index idx)
        {
            _cli.AddInfo(idx.Name, "Finish work");
            _types.Remove(idx.Name);
        }

        public void StartImport() => _cli.AddInfo("", "Start import");

        public void FinishImport() => _cli.AddInfo("", "Finish import");
    }
}