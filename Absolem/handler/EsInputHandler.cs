using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Elasticsearch.Net;
using Nest;
using Serilog.Core;

namespace Absolem.handler
{
    public class EsInputHandler : IInputHandler 
    {
        /***************************************************************************************************************
         *  Properties
         **************************************************************************************************************/
        
        private readonly Logger _log = Program.GetLogger();
        
        private readonly ElasticClient _hlc;
        
        private CliInterface _cli;

        private readonly string _uri;
        
        /// <summary>
        /// The amount of documents to retrieve from the Elasticsearch source at each page
        /// </summary>
        private readonly int _esChunkSize;
        
        /***************************************************************************************************************
         *  Methods
         **************************************************************************************************************/
        
        public EsInputHandler(string uri, int chunkSize)
        {
            ConnectionSettings settings = new ConnectionSettings(new Uri(uri));

            _hlc = new ElasticClient(settings);
            IPingResponse pingResponse = _hlc.Ping();

            if (!pingResponse.ApiCall.Success)
            {
                _log.Fatal("Could not connect to {0}", uri);
                Environment.Exit(1);
            }

            _uri = uri;
            _esChunkSize = chunkSize;
        }

        public void SetCli(CliInterface cli)
        {
            _cli = cli;

            IClusterStatsResponse stats = _hlc.ClusterStats(new ClusterStatsRequest());
            string clusterName    = stats.ClusterName;
            string clusterVersion = stats.Nodes.Versions.Min();

            _cli.Source = $"{_uri} {clusterVersion}";
            _cli.AddInfo("", $"Cluster \"{clusterName}\" v {clusterVersion}");
        }

        public List<Index> GetIndices(List<string> filter)
        {
            List<Index> indices = new List<Index>();
            
            _log.Information("|{0,50}|{3,7}|{4,10}|{2,7}|", "Index", "Health", "Status", "Docs", "Size");
            ICatResponse<CatIndicesRecord> catIndices = _hlc.CatIndices(new CatIndicesRequest());

            foreach (CatIndicesRecord record in catIndices.Records)
            {
                if (filter != null && filter.Count != 0 && !filter.Any(f => Regex.IsMatch(record.Index, $"^{f}$")))
                {
                    continue;
                }

                _cli.AddIdx();
                _cli.AddDocs(long.Parse(record.DocsCount));
                
                _log.Information("|{0,50}|{3,7}|{4,10}|{2,7}|", record.Index, record.Health, record.Status,
                    record.DocsCount, record.StoreSize);
                
                if (record.Status.ToLower() != "open")
                {
                    _cli.AddErr(record.Index, $"{record.Status} index will be ignored");

                    continue;
                }
                    
                indices.Add(new Index(record.Index, record.DocsCount, record.StoreSize));
            }
            
            return indices;
        }

        public string GetClusterName() => _hlc.ClusterStats(new ClusterStatsRequest()).ClusterName;

        public string GetAliases(Index idx) => _hlc.LowLevel.IndicesGetAlias<StringResponse>(idx.Name).Body;
        
        public string GetSettings(Index idx) => _hlc.LowLevel.IndicesGetSettings<StringResponse>(idx.Name).Body;

        public string GetMappings(Index idx) => _hlc.LowLevel.IndicesGetMapping<StringResponse>(idx.Name).Body;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idx"></param>
        /// <param name="dataSink"></param>
        public Task GetData(Index idx, IOutputHandler dataSink)
        {
            SearchRequestParameters searchRequest = new SearchRequestParameters();
            searchRequest.Scroll = TimeSpan.FromMinutes(1);

            DynamicResponse response = _hlc.LowLevel.Search<DynamicResponse>(idx.Name, PostData.Serializable(
                    new
                    {
                        size = _esChunkSize,
                        query = new
                        {
                            match_all = new {}
                        }
                    }), 
                searchRequest);


            while (response.Success && ((List<dynamic>)response.Body["hits"]["hits"]).Any())
            {
                List<dynamic> data = ((List<dynamic>) response.Body["hits"]["hits"]);
                
                foreach (dynamic hit in data)
                {
                    dataSink.AddData(hit._index,  hit._id, hit._source.ToString());
                }

                response = _hlc.LowLevel.Scroll<DynamicResponse>(PostData.Serializable(
                    new 
                    {
                        scroll = "1m",
                        scroll_id = response.Body["_scroll_id"].ToString()
                    }));
            }

            _hlc.LowLevel.ClearScroll<DynamicResponse>(PostData.Serializable(new {
                    scroll_id = response.Body["_scroll_id"].ToString()
            }));

            return Task.CompletedTask;
        }

        public void StartIndexExport(Index idx)
        {
            // Nothing to do here
        }

        public void FinishIndexExport(Index idx)
        {
            // Nothing to do here
        }

        public void StartExport() => _cli.AddInfo("", "Start export from ES");

        public void FinishExport() => _cli.AddInfo("", "Finish export from ES");
    }
}