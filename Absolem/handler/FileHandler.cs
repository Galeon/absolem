using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ionic.Zip;
using ZipFile = Ionic.Zip.ZipFile;

namespace Absolem.handler
{
    public class FileHandler : IInputHandler, IOutputHandler
    {
        
        
        private CliInterface _cli;

        private readonly string _basePath;

        private static ZipOutputStream _zos;

        private static ZipFile _zipFileRead;
        
        private static FileStream _zipFs;

        private const string DataFile = "data.json.gz";
        private const string MappingFile = "mapping.json.gz";
        private const string MetaFile = "meta.json.gz";
        private const string SettingsFile = "settings.json.gz";
        private const string AliasesFile = "aliases.json.gz";
        
        /// <summary>
        /// Holds all streams for actively worked on indices
        /// </summary>
        private readonly ConcurrentDictionary<string, FileStream> _activeFileStreams = 
            new ConcurrentDictionary<string, FileStream>();
        
        private readonly ConcurrentDictionary<string, GZipStream> _activeGZipStreams = 
            new ConcurrentDictionary<string, GZipStream>();
        
        public FileHandler(string source)
        {
            _basePath = source;
            
            // Create the tmp directory
            
        }
        
        public void SetCli(CliInterface cli)
        {
            _cli = cli;
            
            if (string.IsNullOrEmpty(_cli.Source))
            {
                _cli.Source = _basePath;
            }
            else
            {
                cli.Destination = _basePath;
            }
        }
        
        public List<Index> GetIndices(List<string> filter)
        {
            // Much quicker for ramp up than reading every meta file 
            List<Index> indices = _zipFileRead.Entries
                .Select(e => e.FileName.Split("/")[0]) // Get all index names by using the folder name
                .Distinct() // Remove duplicates
                .Select(n => new Index(n, "0", "0")) // Create index objects
                .ToList();

            // Apply index filter
            if (filter != null && filter.Count > 0)
            {
                indices = indices.Where(e => 
                        filter.Any(f => Regex.IsMatch(e.Name, $"^{f}$"))
                        )
                    .ToList(); // Apply filter
            }
            
            _cli.AddIdx(indices.Count);
            return indices;
        }

        private static string ExtractSingleGzipContent(string idx, string filename)
        {
            using MemoryStream mem = new MemoryStream();
            using GZipStream gzip = new GZipStream(
                File.OpenRead(Path.Combine(Program.TmpFolder, idx, filename)), CompressionMode.Decompress
            );
            
            gzip.CopyTo(mem);
            
            return Encoding.UTF8.GetString(mem.ToArray());
        }
        
        public string GetSettings(Index idx)
        {
            return ExtractSingleGzipContent(idx.Name, SettingsFile);
        }

        public string GetMappings(Index idx)
        {
            return ExtractSingleGzipContent(idx.Name, MappingFile);
        }

        public Task GetData(Index idx, IOutputHandler dataSink)
        {
            GZipStream gzip = new GZipStream(File.OpenRead(Path.Combine(Program.TmpFolder, idx.Name, DataFile)), 
                CompressionMode.Decompress);

            StreamReader reader = new StreamReader(gzip, Encoding.UTF8);
            
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (string.IsNullOrEmpty(line)) continue;

                // Extract the id 
                string id = line.Substring(0, line.IndexOf('{'));
                line = line.Remove(0, id.Length);
                
                dataSink.AddData(idx.Name, id, line);
                _cli.FinishedWorkDoc(1);
            }

            return Task.CompletedTask;
        }

        public string GetClusterName()
        {
            throw new NotImplementedException("This is a zip");
        }

        public string GetAliases(Index idx)
        {
            return ExtractSingleGzipContent(idx.Name, AliasesFile);
        }

        public bool AddIndex(Index idx, string settingsJson, bool force)
        {
            AddToZip(GetFilename(idx.Name, SettingsFile), settingsJson);
            AddToZip(GetFilename(idx.Name, MetaFile), idx.ToJson());

            return true;
        }

        public void AddMappings(Index idx, string mappingsJson)
        {
            AddToZip(GetFilename(idx.Name, MappingFile), mappingsJson);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void AddAliases(Index idx, string aliasJson)
        {
            AddToZip(GetFilename(idx.Name, AliasesFile), aliasJson);
        }
        
        public void AddData(string idx, string id, string source)
        {
            AddToZip(GetFilename(idx, DataFile), $"{id}{source}\n");
            _cli.FinishedWorkDoc(1);
        }

        private static string GetFilename(string idx, string type)
        {
            return $"{idx}_{type}";
        }
        
        public void StartIndexImport(Index idx)
        {
            _cli.AddInfo(idx.Name, "Start work");
            
            string dataFileName     = GetFilename(idx.Name, DataFile);
            string mappingFileName  = GetFilename(idx.Name, MappingFile);
            string settingsFileName = GetFilename(idx.Name, SettingsFile);
            string aliasesFileName  = GetFilename(idx.Name, AliasesFile);
            string metaFileName     = GetFilename(idx.Name, MetaFile);

            FileStream dataFs     = File.Create(Path.Combine(Program.TmpFolder, dataFileName));
            FileStream mappingFs  = File.Create(Path.Combine(Program.TmpFolder, mappingFileName));
            FileStream settingsFs = File.Create(Path.Combine(Program.TmpFolder, settingsFileName));
            FileStream aliasesFs  = File.Create(Path.Combine(Program.TmpFolder, aliasesFileName));
            FileStream metaFs     = File.Create(Path.Combine(Program.TmpFolder, metaFileName));
            
            // Store the file input streams
            _activeFileStreams.TryAdd(dataFileName, dataFs);
            _activeFileStreams.TryAdd(mappingFileName, mappingFs);
            _activeFileStreams.TryAdd(settingsFileName, settingsFs);
            _activeFileStreams.TryAdd(aliasesFileName, aliasesFs);
            _activeFileStreams.TryAdd(metaFileName, metaFs);
            
            // Create and store the gzip streams
            _activeGZipStreams.TryAdd(dataFileName    , new GZipStream(dataFs, CompressionMode.Compress));
            _activeGZipStreams.TryAdd(mappingFileName , new GZipStream(mappingFs, CompressionMode.Compress));
            _activeGZipStreams.TryAdd(settingsFileName, new GZipStream(settingsFs, CompressionMode.Compress));
            _activeGZipStreams.TryAdd(aliasesFileName , new GZipStream(aliasesFs, CompressionMode.Compress));
            _activeGZipStreams.TryAdd(metaFileName    , new GZipStream(metaFs, CompressionMode.Compress));
        }
        public void FinishIndexImport(Index idx)
        {
            _cli.AddInfo(idx.Name, "Finish work");
            
            string dataFileName     = GetFilename(idx.Name, DataFile);
            string mappingFileName  = GetFilename(idx.Name, MappingFile);
            string settingsFileName = GetFilename(idx.Name, SettingsFile);
            string aliasesFileName  = GetFilename(idx.Name, AliasesFile);
            string metaFileName     = GetFilename(idx.Name, MetaFile);

            GZipStream stream;

            _activeGZipStreams.Remove(dataFileName    , out stream);
            stream?.Close();
            _activeGZipStreams.Remove(mappingFileName , out stream);
            stream?.Close();
            _activeGZipStreams.Remove(settingsFileName, out stream);
            stream?.Close();
            _activeGZipStreams.Remove(aliasesFileName , out stream);
            stream?.Close();
            _activeGZipStreams.Remove(metaFileName    , out stream);
            stream?.Close();

            FileStream fsStream;
            _activeFileStreams.Remove(dataFileName    , out fsStream);
            fsStream?.Close();
            _activeFileStreams.Remove(mappingFileName , out fsStream);
            fsStream?.Close();
            _activeFileStreams.Remove(settingsFileName, out fsStream);
            fsStream?.Close();
            _activeFileStreams.Remove(aliasesFileName , out fsStream);
            fsStream?.Close();
            _activeFileStreams.Remove(metaFileName    , out fsStream);
            fsStream?.Close();

            ZipFile(idx.Name, Path.Combine(Program.TmpFolder, mappingFileName));
            ZipFile(idx.Name, Path.Combine(Program.TmpFolder, dataFileName));
            ZipFile(idx.Name, Path.Combine(Program.TmpFolder, settingsFileName));
            ZipFile(idx.Name, Path.Combine(Program.TmpFolder, aliasesFileName));
            ZipFile(idx.Name, Path.Combine(Program.TmpFolder, metaFileName));
        }

        public void StartIndexExport(Index idx)
        {
            _cli.AddInfo(idx.Name, $"Extracting ZIP data");
            ExtractIndexFiles(idx.Name);
        }

        public void FinishIndexExport(Index idx)
        {
            Directory.Delete(Path.Combine(Program.TmpFolder, idx.Name), true);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void ZipFile(string idx, string file)
        {
            _zos.PutNextEntry($"{idx}/{file.Remove(0, idx.Length + Program.TmpFolder.Length + 2)}");
            _zos.Write(File.ReadAllBytes(file));
            File.Delete(file);
        }

        public void StartExport()
        {
            PrepareZipRead(_basePath);
        }

        private static void PrepareZipWrite(string path)
        {
            _zipFs = File.Create(path);
            _zos = new ZipOutputStream(_zipFs);
        }

        /// <summary>
        /// Prepare the properties for stream reading from the zip archive
        /// </summary>
        /// <param name="path"></param>
        private static void PrepareZipRead(string path)
        {
            _zipFileRead = Ionic.Zip.ZipFile.Read(path);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void FinishExport()
        {
            _cli.AddInfo("", "Finish export");
            
            _zipFs?.Close();
            _zipFileRead.Dispose();
        }

        public void StartImport()
        {
            _cli.AddInfo("", "Start import to ZIP");
            PrepareZipWrite(_basePath);
        }

        public void FinishImport()
        {
            _cli.AddInfo("", "Finish import to ZIP");

            _zos?.Close();
            _zipFs?.Close();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void ExtractIndexFiles(string idx)
        {
            _zipFileRead.ExtractSelectedEntries($"name = *", $"{idx}", 
                Program.TmpFolder, ExtractExistingFileAction.OverwriteSilently);
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        private void AddToZip(string filename, string data)
        {
            _activeGZipStreams.TryGetValue(filename, out GZipStream stream);

            if (stream != null)
            {
                byte[] bytes = Encoding.UTF8.GetBytes(data);
                stream.Write(bytes);
            }
            else
            {
                _cli.AddErr(filename, "Could not get stream to write");
            }
        }
    }
}