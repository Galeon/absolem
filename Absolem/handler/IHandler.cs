namespace Absolem.handler
{
    public interface IHandler
    {
        /// <summary>
        /// Set the CLI interface on the handler
        /// </summary>
        /// <param name="cli">The CLI instance</param>
        public void SetCli(CliInterface cli);
        
        /// <summary>
        /// Retrieves the name of the ES cluster
        /// </summary>
        /// <returns></returns>
        string GetClusterName();
    }
}