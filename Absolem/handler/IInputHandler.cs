using System.Collections.Generic;
using System.Threading.Tasks;

namespace Absolem.handler
{
    /// <summary>
    /// Contains all the methods required for getting data out of a source
    /// </summary>
    public interface IInputHandler : IHandler
    {
        /// <summary>
        /// Get a list of all available indices in the data source
        /// </summary>
        /// <param name="filter">A list of regex filter to apply to the list</param>
        /// <returns>A list of indices</returns>
        List<Index> GetIndices(List<string> filter);
        
        /// <summary>
        /// Retrieves the json for the settings of the given index
        /// </summary>
        /// <param name="idx">The name of the index</param>
        /// <returns></returns>
        string GetSettings(Index idx);

        /// <summary>
        /// Retrieve the JSON mappings for the index
        /// </summary>
        /// <param name="idx">The name of the index</param>
        /// <returns></returns>
        string GetMappings(Index idx);
        
        /// <summary>
        /// Implementations of this method should read all data from the given index and pass them to the data sink
        /// by calling AddData.
        /// </summary>
        /// <param name="idx">The name of the index</param>
        /// <param name="dataSink">The output handler which writes the data</param>
        Task GetData(Index idx, IOutputHandler dataSink);

        /// <summary>
        /// Retrieves all aliases for a given index
        /// </summary>
        /// <param name="idx">The name of the index</param>
        /// <returns>A list of alias names</returns>
        string GetAliases(Index idx);
        
        /// <summary>
        /// Gets fired when the export of an index starts
        /// </summary>
        /// <param name="idx">The name of the index</param>
        void StartIndexExport(Index idx);

        /// <summary>
        /// Gets fired when the export of an index is finished
        /// </summary>
        /// <param name="idx">The name of the index</param>
        void FinishIndexExport(Index idx);

        /// <summary>
        /// Gets fired when the whole export process starts
        /// </summary>
        void StartExport();

        /// <summary>
        /// Gets fired when the whole export process is finished
        /// </summary>
        void FinishExport();
    }
}